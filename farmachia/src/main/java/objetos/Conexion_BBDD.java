
package objetos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Conexion_BBDD {
    
    Connection conexion = null;
    Statement statement;
         
    public Statement conexion(){

        try{
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:temp.db";
            
            conexion = DriverManager.getConnection(url);
            
            statement = conexion.createStatement();
            System.out.println("Conectado correctamente.");
            ResultSet rs = statement.executeQuery("SELECT * from ExcipientesSimples");
            
            while(rs.next()){
                System.out.println("id : " + rs.getString("nombre"));
                System.out.println("sex : " + rs.getString("precio"));
            
            }
        }catch(SQLException ex){
            System.out.println("ERROR: No se ha podido conectar con el servidor.");
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion_BBDD.class.getName()).log(Level.SEVERE, null, ex);
        }
         return statement;
    }
}
